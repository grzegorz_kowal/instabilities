--------------------------------------------------------------------------------
# **The INSTABILITIES Code**
## Copyright (C) 2015-2019 Grzegorz Kowal ##
--------------------------------------------------------------------------------

INSTABILITIES is a tool to analyze magnetohydrodynamic instabilities in data
cubes from simulations done by Godunov or Amun codes.

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
this program.  If not, see <http://www.gnu.org/licenses/>.


Developers
==========

 - Grzegorz Kowal <grzegorz@amuncode.org>


Requirements
============

* Fortran 2003 compiler, e.g.  [GNU Fortran](http://gcc.gnu.org/fortran/)
  version 4.5 or newer,
* [HDF5 libraries](http://www.hdfgroup.org/HDF5/) version 1.8 or newer,
* [CFITSIO libraries](https://heasarc.gsfc.nasa.gov/docs/software/fitsio/)
  version 3.x,
* [GNU Scientific Library](https://www.gnu.org/software/gsl/) version 1.16
  through its Fortran bindings
  [FGSL](https://www.lrz.de/services/software/mathematik/gsl/fortran/)
  version 1.0.0.
