!!******************************************************************************
!!
!!  This file is part of the source code INSTABILITIES, a program to analyze
!!  magnetohydrodynamic (MHD) instabilities in data cubes of numerical
!!  simulations.
!!
!!  Copyright (C) 2017-2019 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! Program: INSTABILITIES
!!
!!  Instabilities is a tool to analyze magnetohydrodynamic (MHD) instabilities
!!  in data cubes obtained from numerical simulations done with the help of
!!  Amun and Godunov codes.
!!
!!******************************************************************************
!
program instabilities

! external module subroutines and variables
!
  use coordinates, only : initialize_coordinates
  use detection  , only : detect_tearing, detect_tearing_amr
  use detection  , only : detect_kelvinhelmholtz, detect_kelvinhelmholtz_amr
  use fields     , only : initialize_fields, finalize_fields, amr
  use parameters , only : read_parameters, finalize_parameters
  use parameters , only : get_parameter

! program variables are not implicit by default
!
  implicit none

! instability name to analyze
!
  character(len=256) :: instability = 'tearing'

! status flag
!
  integer :: status = 0
!
!-------------------------------------------------------------------------------
!
! print welcome information
!
  write (*,"(1x,78('-'))")
  write (*,"(1x,18('='),14x,a,15x,18('='))") 'INSTABILITIES'
  write (*,"(1x,18('='), 2x,a, 2x,18('='))") 'Copyright (C) 2017-2019 Grzegorz Kowal'
  write (*,"(1x,18('='), 9x,a,10x,18('='))") 'under GNU GPLv3 license'
  write (*,"(1x,78('-'))")

! read parameters from the parameter file
!
  call read_parameters(status)
  if (status /= 0) goto 300

! initialize module FIELDS
!
  call initialize_fields(status)
  if (status /= 0) goto 200

! initialize module COORDINATES
!
  call initialize_coordinates(status)
  if (status /= 0) goto 100

! get detection method name
!
  call get_parameter("instability", instability)

! perform the selected instability analysis
!
  select case(trim(instability))
  case('tr', 'tearing', 'tearing-mode')
    if (amr) then
      call detect_tearing_amr()
    else
      call detect_tearing()
    end if
  case('kh', 'kelvin-helmholtz', 'KH', 'Kelvin-Helmholtz')
    if (amr) then
      call detect_kelvinhelmholtz_amr()
    else
      call detect_kelvinhelmholtz()
    end if
  case default
    write( *, "('INFO      : ',a)" ) "no instability chosen"
  end select

  100 continue

! finalize fields
!
  200 continue
  call finalize_fields(status)

! finalize parameters
!
  300 continue
  call finalize_parameters()

!-------------------------------------------------------------------------------
!
end program instabilities
!
 !===============================================================================
