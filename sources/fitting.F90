!!******************************************************************************
!!
!!  This file is part of the source code INSTABILITIES, a program to analyze
!!  magnetohydrodynamic (MHD) instabilities in data cubes of numerical
!!  simulations.
!!
!!  Copyright (C) 2017-2019 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! module: FITTING
!!
!!  This module provides subroutines to fit functions to data.
!!
!!******************************************************************************
!
module fitting

! external module subroutines and variables
!
  use fgsl
  use, intrinsic :: iso_c_binding

! module variables are not implicit by default
!
  implicit none

! structure to hold the fitting data
!
  type data
     integer(fgsl_size_t)           :: n
     real(fgsl_double), allocatable :: x(:), y(:), s(:)
  end type data

! by default everything is private
!
  private

! declare public subroutines
!
  public :: tanh_fit

!- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
!
  contains
!
!===============================================================================
!
! function TANH_F:
! ---------------
!
!   Function calculates function
!
!     f(x) = x₁·tanh((x - x₂) / x₃) + x₄
!
!   for a given x, where x₁, x₂, x₃, and x₄ are fitting parameters.
!
!
!===============================================================================
!
  function tanh_f(x, cdata, f) bind(c)

! local variables are not implicit by default
!
    implicit none

! function arguments
!
    type(c_ptr)   , value :: x, cdata, f
    integer(c_int)        :: tanh_f

! local variables
!
    type(fgsl_vector)          :: f_x, f_f
    type(data), pointer        :: f_data
    integer(fgsl_size_t)       :: i
    integer(fgsl_int)          :: status
    real(fgsl_double)          :: yy
    real(fgsl_double), pointer :: p_x(:), p_f(:)
!
!-------------------------------------------------------------------------------
!
    call fgsl_obj_c_ptr(f_x, x)
    call fgsl_obj_c_ptr(f_f, f)

    status = fgsl_vector_align(p_x, f_x)
    status = fgsl_vector_align(p_f, f_f)

    call c_f_pointer(cdata, f_data)

    do i = 1, f_data%n
       yy = p_x(1) * tanh((f_data%x(i) - p_x(2)) / p_x(3)) + p_x(4)
       p_f(i) = (yy - f_data%y(i)) / f_data%s(i)
    end do

    tanh_f = fgsl_success

!-------------------------------------------------------------------------------
!
  end function tanh_f
!
!===============================================================================
!
! function TANH_DF:
! ----------------
!
!   Function calculates the Jacobian of function
!
!     f(x) = x₁·tanh((x - x₂) / x₃) + x₄
!
!   for a given x.
!
!
!===============================================================================
!
  function tanh_df(x, cdata, j) bind(c)

! local variables are not implicit by default
!
    implicit none

! function arguments
!
    type(c_ptr)   , value :: x, cdata, j
    integer(c_int)        :: tanh_df

! local variables
!
    type(fgsl_vector)          :: f_x
    type(fgsl_matrix)          :: f_j
    type(data)       , pointer :: f_data
    integer(fgsl_size_t)       :: i
    integer(fgsl_int)          :: status
    real(fgsl_double)          :: xx, yy
    real(fgsl_double), pointer :: p_x(:), p_j(:,:)
!
!-------------------------------------------------------------------------------
!
    call fgsl_obj_c_ptr(f_x, x)
    call fgsl_obj_c_ptr(f_j, j)

    status = fgsl_vector_align(p_x, f_x)
    status = fgsl_matrix_align(p_j, f_j)

    call c_f_pointer(cdata, f_data)

    do i = 1, f_data%n
       xx = (f_data%x(i) - p_x(2)) / p_x(3)
       yy = - p_x(1) / p_x(3) / cosh(xx)**2
       p_j(1,i) = tanh(xx)
       p_j(2,i) = yy
       p_j(3,i) = xx * yy
       p_j(4,i) = 1.0_fgsl_double
       p_j(:,i) = p_j(:,i) / f_data%s(i)
    end do

    tanh_df = fgsl_success

!-------------------------------------------------------------------------------
!
  end function tanh_df
!
!===============================================================================
!
! function TANH_FDF:
! -----------------
!
!   Function to store components of second directional derivatives of
!   the function to be minimized.
!
!
!===============================================================================
!
  function tanh_fdf(x, cdata, f, j) bind(c)

! local variables are not implicit by default
!
    implicit none

! function arguments
!
    type(c_ptr)   , value :: x, cdata, f, j
    integer(c_int)        :: tanh_fdf

! local variables
!
    integer(c_int) :: status
!
!-------------------------------------------------------------------------------
!
    status = tanh_f (x, cdata, f)
    status = tanh_df(x, cdata, j)

    tanh_fdf = fgsl_success

!-------------------------------------------------------------------------------
!
  end function tanh_fdf
!
!===============================================================================
!
! subroutine TANH_FIT:
! -------------------
!
!   This is the main subroutine to perform fitting of the function
!
!     y = f(x) = x₁·tanh((x - x₂) / x₃) + x₄
!
!   to a sequence of (x, y) pairs.
!
!   Arguments:
!
!     n - the length of vectors x and y;
!     x - the position vector;
!     y - the vector of function values;
!     p - the vector of fitted parameters;
!     s - the status flag indicating if the fitting was successful;
!
!===============================================================================
!
  subroutine tanh_fit(n, x, y, p, c, ci, s)

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    integer                   , intent(in)  :: n
    real(kind=8), dimension(n), intent(in)  :: x, y
    real(kind=8), dimension(n), intent(out) :: p, c
    real(kind=8)              , intent(out) :: ci
    logical                   , intent(out) :: s

! local variables
!
    integer(fgsl_size_t)             :: nmax, nrt
    integer(fgsl_int)                :: ret
    type(fgsl_multifit_function_fdf) :: nlfit_fdf
    type(fgsl_multifit_fdfsolver)    :: nlfit_slv
    type(fgsl_vector)                :: params
    type(fgsl_matrix)                :: cov, jac
    type(data), target               :: fitdata
    type(c_ptr)                      :: ptr
    real(fgsl_double), target        :: v_params(4), v_cov(4,4), v_jac(n,4)
    real(fgsl_double), pointer       :: v_fun(:), v_parptr(:)
    real(fgsl_double)                :: chi, cc
    integer                          :: i

! local parameters
!
    real(fgsl_double)   , parameter :: eps   = 1.0E-4_fgsl_double
    integer(fgsl_size_t), parameter :: itmax = 30
!
!-------------------------------------------------------------------------------
!
! prepare the structure for fitted data
!
    nmax = n
    fitdata%n = nmax
    allocate(fitdata%x(nmax),fitdata%y(nmax),fitdata%s(nmax))
    do i = 1, nmax
       fitdata%x(i) = x(i)
       fitdata%y(i) = y(i)
       fitdata%s(i) = 1.0d+00 / dble(nmax)
    end do
    ptr = c_loc(fitdata)
    nrt = 4_fgsl_size_t

! pass the pointer to the fitted functions
!
    nlfit_fdf = fgsl_multifit_function_fdf_init(tanh_f, tanh_df, tanh_fdf      &
                                                               , nmax, nrt, ptr)

! select the solver
!
    nlfit_slv = fgsl_multifit_fdfsolver_alloc(fgsl_multifit_fdfsolver_lmsder   &
                                                               , nmax, nrt)

! set the initial guess for fit parameters
!
    v_params(1:4) = (/ 1.0_fgsl_double, 0.0_fgsl_double                        &
                     , 1.0_fgsl_double, 0.0_fgsl_double /)

    params = fgsl_vector_init(type = 1.0_fgsl_double)
    ret    = fgsl_vector_align(v_params, nrt, params,nrt                       &
                                               , 0_fgsl_size_t, 1_fgsl_size_t)

! alignment of target function and parameter values only needed once
! storage allocated within the nlfit_slv object
!
    ret    = fgsl_vector_align(v_fun, fgsl_multifit_fdfsolver_f(nlfit_slv))
    ret    = fgsl_vector_align(v_parptr                                        &
                                , fgsl_multifit_fdfsolver_position(nlfit_slv))

! storage for cov within Fortran
!
    cov    = fgsl_matrix_init(type = 1.0_fgsl_double)
    ret    = fgsl_matrix_align(v_cov,nrt,nrt,nrt,cov)
    jac    = fgsl_matrix_init(type = 1.0_fgsl_double)
    ret    = fgsl_matrix_align(v_jac,nrt,nrt,nmax,jac)

! if everything is ok, perform the fitting
!
    if (fgsl_well_defined(nlfit_slv)) then

      ret = fgsl_multifit_fdfsolver_set(nlfit_slv, nlfit_fdf, params)

! iterate until the convergence is reached
!
      i = 0
      do
         i = i + 1
         ret = fgsl_multifit_fdfsolver_iterate(nlfit_slv)
         if (ret /= fgsl_success .or. i > itmax) then
           s = .false.
           exit
         end if
         ret = fgsl_multifit_test_delta(                                       &
                        fgsl_multifit_fdfsolver_dx(nlfit_slv),                 &
                        fgsl_multifit_fdfsolver_position(nlfit_slv), eps, eps)
         if (ret == fgsl_success) then
            s = .true.
            exit
         end if
      end do

! get the covariance and error estimations
!
      ret = fgsl_multifit_fdfsolver_jac(nlfit_slv, jac)
      ret = fgsl_multifit_covar(jac, 0.0_fgsl_double, cov)
      chi = sqrt(dot_product(v_fun,v_fun))
      cc  = max(1.0d+00, chi / sqrt(dble(nmax - nrt)))

! copy the estimated parameter values
!
      do i = 1, 4
        p(i) = v_parptr(i)
        c(i) = cc * sqrt(v_cov(i,i))
      end do
      ci = (chi / nmax)**2 / nmax

      s = .true.

    else

      s = .false.

    end if

! finalize fitting
!
    call fgsl_vector_free(params)
    call fgsl_matrix_free(cov)
    call fgsl_matrix_free(jac)
    call fgsl_multifit_fdfsolver_free(nlfit_slv)
    call fgsl_multifit_function_fdf_free(nlfit_fdf)

!-------------------------------------------------------------------------------
!
  end subroutine tanh_fit

!===============================================================================
!
end module fitting
