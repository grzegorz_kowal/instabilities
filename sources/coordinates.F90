!!******************************************************************************
!!
!!  This file is part of the source code INSTABILITIES, a program to analyze
!!  magnetohydrodynamic (MHD) instabilities in data cubes of numerical
!!  simulations.
!!
!!  Copyright (C) 2017-2019 Grzegorz Kowal <grzegorz@amuncode.org>
!!
!!  This program is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  This program is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!!  GNU General Public License for more details.
!!
!!  You should have received a copy of the GNU General Public License
!!  along with this program.  If not, see <http://www.gnu.org/licenses/>.
!!
!!******************************************************************************
!!
!! module: COORDINATES
!!
!!  This module provides subroutines for coordinates.
!!
!!******************************************************************************
!
module coordinates

! module variables are not implicit by default
!
  implicit none

! periodicity
!
  logical     , dimension(3)  , save :: periodic = .false.

! grid sizes
!
  real(kind=8), dimension(3)  , save :: dr = 1.0d+00

! domain limits
!
  real(kind=8), dimension(3,2), save :: dl = 0.0d+00

! by default everything is private
!
  private

! declare public subroutines
!
  public :: initialize_coordinates
  public :: get_periodicity, get_domain_limit
  public :: get_cell_size, get_cell_coordinate, get_cell_index
!
!-------------------------------------------------------------------------------
!
  contains
!
!===============================================================================
!
! subroutine INITIALIZE_COORDINATES:
! ---------------------------------
!
!   Subroutine initializes module parameters and variables.
!
!   Arguments:
!
!     iret - the return value; if it is 0 everything went successfully,
!            otherwise there was a problem;
!
!===============================================================================
!
  subroutine initialize_coordinates(iret)

! external module subroutines and variables
!
    use hdf5io    , only : hdf5_get_cellsize, hdf5_get_domain_limits
    use parameters, only : get_parameter

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    integer, intent(inout) :: iret

! local variables
!
    character(len= 32) :: fformat = "bin"
    character(len= 32) :: fbndry  = "periodic"
!
!-------------------------------------------------------------------------------
!
! get the format of the input file
!
    call get_parameter("fformat" , fformat)

! get the cell sizes
!
    write( *, "('INFO      : ',a)" ) "reading cell sizes and domain limits"
    select case(fformat)
#ifdef HDF5
    case('hdf5', 'HDF5', 'h5', 'H5')
      call hdf5_get_cellsize(dr(:))
      call hdf5_get_domain_limits(dl(:,:))
#endif /* HDF5 */
    case default
      return
    end select

! set the mapping subroutine pointes
!
    call get_parameter("xbndry", fbndry)
    select case(fbndry)
    case('periodic')
      periodic(1) = .true.
    end select
    call get_parameter("ybndry", fbndry)
    select case(fbndry)
    case('periodic')
      periodic(2) = .true.
    end select
    call get_parameter("zbndry", fbndry)
    select case(fbndry)
    case('periodic')
      periodic(3) = .true.
    end select

!-------------------------------------------------------------------------------
!
  end subroutine initialize_coordinates
!
!===============================================================================
!
! function GET_PERIODICITY:
! ------------------------
!
!   Function returns the periodicity in a given direction.
!
!   Arguments:
!
!     idir  - the direction;
!     iside - the side of the limit;
!
!===============================================================================
!
  logical function get_periodicity(idir) result(per)

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    integer, intent(in) :: idir
!
!-------------------------------------------------------------------------------
!
! return the periodicity flag
!
    per = periodic(idir)

!-------------------------------------------------------------------------------
!
  end function get_periodicity
!
!===============================================================================
!
! function GET_DOMAIN_LIMIT:
! -------------------------
!
!   Function returns the cell size in a given direction.
!
!   Arguments:
!
!     idir  - the direction;
!     iside - the side of the limit;
!
!===============================================================================
!
  real(kind=8) function get_domain_limit(idir, iside) result(dlim)

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    integer, intent(in) :: idir, iside
!
!-------------------------------------------------------------------------------
!
! return the domain limit
!
    dlim = dl(idir, iside)

!-------------------------------------------------------------------------------
!
  end function get_domain_limit
!
!===============================================================================
!
! function GET_CELL_SIZE:
! ----------------------
!
!   Function returns the cell size in a given direction.
!
!   Arguments:
!
!     idir - the direction;
!
!===============================================================================
!
  real(kind=8) function get_cell_size(idir) result(csize)

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    integer, intent(in) :: idir
!
!-------------------------------------------------------------------------------
!
! reset the inside flag
!
    csize = dr(idir)

!-------------------------------------------------------------------------------
!
  end function get_cell_size
!
!===============================================================================
!
! function GET_CELL_COORDINATE:
! ----------------------------
!
!   Function returns the cell position from its index in a given direction.
!
!   Arguments:
!
!     idir  - the direction;
!     icell - the cell index;
!
!===============================================================================
!
  real(kind=8) function get_cell_coordinate(idir, icell) result(x)

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    integer, intent(in) :: idir, icell
!
!-------------------------------------------------------------------------------
!
! return the cell coordinate
!
    x = dr(idir) * (1.0d+00 * icell - 0.5d+00) + dl(idir, 1)

!-------------------------------------------------------------------------------
!
  end function get_cell_coordinate
!
!===============================================================================
!
! subroutine GET_CELL_INDEX:
! -------------------------
!
!   Subroutine returns left-side and right-side cell index for the given
!   position.
!
!   Arguments:
!
!     p  - the coordinate periodicity flag;
!     x  - the input position;
!     nn - the domain resolution;
!     nl - the lower limit for the cell index;
!     nu - the upper limit for the cell index;
!     im - the cell floor index;
!     ip - the cell ceiling index;
!
!===============================================================================
!
  subroutine get_cell_index(p, x, nn, nl, nu, im, ip)

! local variables are not implicit by default
!
    implicit none

! subroutine arguments
!
    logical     , intent(in)  :: p
    real(kind=8), intent(in)  :: x
    integer     , intent(in)  :: nn, nl, nu
    integer     , intent(out) :: im, ip

! local variables
!
    real(kind=8) :: r
!
!-------------------------------------------------------------------------------
!
    if (p) then
      r  = x / nn
      im = floor((r - floor(r)) * nn)
      if (im == nn) then
        ip = 1
      else
        ip = im + 1
      end if
    else
      im = min(max(floor(x), nl), nu)
      ip = min(im + 1, nu)
    end if

!-------------------------------------------------------------------------------
!
  end subroutine get_cell_index

!===============================================================================
!
end module coordinates
